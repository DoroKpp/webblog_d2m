package de.awacademy.webblogD2M.signup;

import de.awacademy.webblogD2M.login.LoginService;
import de.awacademy.webblogD2M.user.User;
import de.awacademy.webblogD2M.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
public class SignupController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    LoginService loginService;

    @GetMapping("registrierung")
    public String registrierung(Model model) {
        model.addAttribute("registrierung", new SignupDTO());
        return "registrierung";
    }

    @PostMapping("registrierung")
    public String registrierung(@ModelAttribute("registrierung") @Valid SignupDTO registrierung, BindingResult bindingResult, HttpServletResponse response) {
        if (!registrierung.getPasswort1().equals(registrierung.getPasswort2())) {
            bindingResult.addError(new FieldError("registrierung", "passwort2", "Die Passwörter stimmen nicht überein"));
        }

        if (userRepository.existsByName(registrierung.getBenutzername())) {
            bindingResult.addError(new FieldError("registrierung", "benutzername", "Benutzername exisitiert bereits"));
        }

        if (bindingResult.hasErrors()) {
            return "registrierung";
        }

        User user = new User(registrierung.getBenutzername(), registrierung.getPasswort1());
        userRepository.save(user);

        loginService.login(user, response);

        return "redirect:/";
    }
}
