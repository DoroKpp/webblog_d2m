package de.awacademy.webblogD2M.impressum;

import de.awacademy.webblogD2M.beitrag.BeitragDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ImpressumController {

    @GetMapping("/impressum")
    public String erstelle(Model model) {
        return "/impressum";
    }
}
