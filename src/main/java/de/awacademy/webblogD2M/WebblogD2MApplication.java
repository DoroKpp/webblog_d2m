package de.awacademy.webblogD2M;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebblogD2MApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebblogD2MApplication.class, args);
	}
}
