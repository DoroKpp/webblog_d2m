package de.awacademy.webblogD2M.home;

import de.awacademy.webblogD2M.beitrag.Beitrag;
import de.awacademy.webblogD2M.beitrag.BeitragDTO;
import de.awacademy.webblogD2M.beitrag.BeitragRepository;
import de.awacademy.webblogD2M.kategorie.Kategorie;
import de.awacademy.webblogD2M.kategorie.KategorieRepository;
import de.awacademy.webblogD2M.kommentar.Kommentar;
import de.awacademy.webblogD2M.kommentar.KommentarDTO;
import de.awacademy.webblogD2M.kommentar.KommentarRepository;
import de.awacademy.webblogD2M.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Controller
public class HomeController {

    @Autowired
    private BeitragRepository beitragRepository;

    @Autowired
    private KommentarRepository kommentarRepository;

    @Autowired
    private KategorieRepository kategorieRepository;

    @GetMapping("/")
    public String home(Model model, @ModelAttribute("currentUser") User currentUser) {
        HomeDTO homeDTO = new HomeDTO();
        for (Kategorie kategorie : kategorieRepository.findAll()) {
            homeDTO.getKategorieIdList().add(kategorie.getId());
        }
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("kommentar", new KommentarDTO());
        model.addAttribute("beitraege", beitragRepository.findAllByOrderByErstellungsDatumDesc());
        model.addAttribute("dateFormatter",  DateTimeFormatter.ofLocalizedDateTime( FormatStyle.SHORT )
                .withLocale( Locale.getDefault() )
                .withZone( ZoneId.systemDefault() ));
        model.addAttribute("kategorien", kategorieRepository.findAll());
        model.addAttribute("homeDTO", homeDTO);
        return "home";
    }

    @GetMapping("/kategorienWaehlen")
    public String katWaehlen(Model model) {
        return "redirect:/";
    }

    @PostMapping("/kategorienWaehlen")
    public String katWaehlen(Model model, @ModelAttribute("currentUser") User currentUser, @ModelAttribute("homeDTO") HomeDTO homeDTO) {
        homeDTO.getKategorieIdList();
        List<Beitrag> beitragList = beitragRepository.findAllByOrderByErstellungsDatumDesc();
        List<Beitrag> beitragListShow = new LinkedList<>();
        for (Beitrag beitrag : beitragList) {
            boolean show = false;
            for (long id : homeDTO.getKategorieIdList()) {
                Optional<Kategorie> optionalKategorie = kategorieRepository.findById(id);
                if (optionalKategorie.isPresent() && beitrag.getKategorien().contains(optionalKategorie.get())) {
                    show = true;
                    break;
                }
            }
            if (show) {
                beitragListShow.add(beitrag);
            }
        }

        model.addAttribute("currentUser", currentUser);
        model.addAttribute("kommentar", new KommentarDTO());
        model.addAttribute("beitraege", beitragListShow);
        model.addAttribute("dateFormatter",  DateTimeFormatter.ofLocalizedDateTime( FormatStyle.SHORT )
                .withLocale( Locale.getDefault() )
                .withZone( ZoneId.systemDefault() ));
        model.addAttribute("kategorien", kategorieRepository.findAll());
        model.addAttribute("homeDTO", homeDTO);
        return "home";
    }

    @PostMapping("/alleZeigen")
    public String alleZeigen() {
        return "redirect:/";
    }

}
