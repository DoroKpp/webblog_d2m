package de.awacademy.webblogD2M.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findFirstByNameAndPasswort(String name, String passwort);

    boolean existsByName(String name);
}
