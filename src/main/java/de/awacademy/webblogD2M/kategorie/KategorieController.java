package de.awacademy.webblogD2M.kategorie;

import de.awacademy.webblogD2M.beitrag.BeitragDTO;
import de.awacademy.webblogD2M.beitrag.BeitragRepository;
import de.awacademy.webblogD2M.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class KategorieController {

    @Autowired
    KategorieRepository kategorieRepository;

    @Autowired
    BeitragRepository beitragRepository;

    @GetMapping("/addKategorie")
    public String create(Model model, @ModelAttribute("currentUser") User currentUser) {
        model.addAttribute("kategorie", new KategorieDTO());
        if (currentUser == null || !currentUser.isAdmin()) {
            return "redirect:/login";
        } else {
            return "addKategorie";
        }
    }

    @GetMapping("getEditAddKategorie")
    public String create(@RequestParam("beitragId") long beitragId, Model model) {
        model.addAttribute("kategorie", new KategorieDTO());
        model.addAttribute("beitragId", beitragId);
        BeitragDTO beitragDTO = new BeitragDTO();
        beitragDTO.setId(beitragId);
        model.addAttribute("beitrag", beitragDTO);
        return "addKategorieEdit";
    }

    @PostMapping("/addKategorie")
    public String create(@ModelAttribute("currentUser") User currentUser, @ModelAttribute("addKategorie") KategorieDTO kategorieDTO) {
        if (currentUser == null) {
            return "redirect:/login";
        } else if (!currentUser.isAdmin()) {
            return "redirect:/login";
        } else {
            Kategorie kategorieEntity = new Kategorie();
            kategorieEntity.setKategorieName(kategorieDTO.getKategorieName());
            kategorieRepository.save(kategorieEntity);
            return "redirect:/beitrag";
        }
    }

    @PostMapping("/editAddKategorie")
    public String create(@RequestParam("beitragId") long beitragIdK, @ModelAttribute KategorieDTO kategorieDTO, Model model) {
        Kategorie kategorieEntity = new Kategorie();
        kategorieEntity.setKategorieName(kategorieDTO.getKategorieName());
        kategorieRepository.save(kategorieEntity);
        String str = "?beitragId=";
        return "redirect:/getEditBeitrag" + str + beitragIdK;
    }
}
