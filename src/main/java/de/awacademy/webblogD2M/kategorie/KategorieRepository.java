package de.awacademy.webblogD2M.kategorie;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface KategorieRepository extends JpaRepository<Kategorie,Long> {
    List<Kategorie> findAllByOrderById();
    List<Kategorie> findAllByOrderByKategorieName();
}
