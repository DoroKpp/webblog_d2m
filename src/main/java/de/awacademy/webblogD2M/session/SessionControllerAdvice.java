package de.awacademy.webblogD2M.session;

import de.awacademy.webblogD2M.user.User;
import de.awacademy.webblogD2M.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Optional;

@ControllerAdvice
public class SessionControllerAdvice {

    @Autowired
    SessionRepository sessionRepository;

    @ModelAttribute("currentUser")
    public User currentUser(@CookieValue(value = "sessionId", defaultValue = "") String sessionId) {
        if (sessionId.length() > 0) {
            Optional<Session> session = sessionRepository.findById(sessionId);
            if (session.isPresent()) {
                return session.get().getUser();
            }
        }
        return null;
    }
}
