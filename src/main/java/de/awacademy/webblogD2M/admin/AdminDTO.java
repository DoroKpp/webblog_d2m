package de.awacademy.webblogD2M.admin;

import javax.validation.constraints.NotEmpty;
import java.util.LinkedList;
import java.util.List;

public class AdminDTO {

    @NotEmpty
    private List<Long> adminIds = new LinkedList<>();

    //Getter / Setter
    public List<Long> getAdminIds() {
        return adminIds;
    }

    public void setAdminIds(List<Long> adminIds) {
        this.adminIds = adminIds;
    }

}
