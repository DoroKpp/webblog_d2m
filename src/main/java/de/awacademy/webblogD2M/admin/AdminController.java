package de.awacademy.webblogD2M.admin;

import de.awacademy.webblogD2M.session.SessionControllerAdvice;
import de.awacademy.webblogD2M.user.User;
import de.awacademy.webblogD2M.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class AdminController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/addAdmin")
    public String admin(Model model, @ModelAttribute("currentUser") User currentUser) {
        SessionControllerAdvice sc = new SessionControllerAdvice();
        AdminDTO adminDTO = new AdminDTO();
        for (User user : userRepository.findAll()) {
            if (user.isAdmin()) {
                adminDTO.getAdminIds().add(user.getId());
            }
        }
        model.addAttribute("users", userRepository.findAll());
        model.addAttribute("adminDTO", adminDTO);
        return "admin";
    }

    @PostMapping("/addAdmin")
    public String addAdmin(Model model, @ModelAttribute("currentUser") User currentUser, @ModelAttribute("adminDTO") AdminDTO adminDTO) {

        List<User> usersEnt = userRepository.findAll();
        model.addAttribute("users", usersEnt);
        if (!currentUser.isAdmin()) {
            return "redirect:/";

        }

        for (User user : usersEnt) {
            if (user == currentUser) {
                user.setAdmin(true);
            } else {
                user.setAdmin(adminDTO.getAdminIds().contains(user.getId()));
            }
        }

        for (User user: usersEnt) {
            userRepository.save(user);
        }
        return "redirect:/";
    }

}
