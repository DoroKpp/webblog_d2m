package de.awacademy.webblogD2M.archivbeitrag;

import de.awacademy.webblogD2M.beitrag.Beitrag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArchivbeitragRepository extends JpaRepository<Archivbeitrag, Long> {
    List<Archivbeitrag> findAllByBeitragOrderByErstellungsDatumDesc(Beitrag beitrag);
    List<Archivbeitrag> findAllByBeitrag(Beitrag beitrag);
}
