package de.awacademy.webblogD2M.archivbeitrag;

import de.awacademy.webblogD2M.beitrag.Beitrag;
import de.awacademy.webblogD2M.beitrag.BeitragRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Controller
public class ArchivbeitragController {

    @Autowired
    ArchivbeitragRepository archivbeitragRepository;

    @Autowired
    BeitragRepository beitragRepository;

    @GetMapping("/showArchiv")
    public String showArchiv(Model model, @RequestParam("beitragId") Long beitragId) {
        //nur die Archivbeiträge zur gewählten beitragId in Liste einfügen
        Optional<Beitrag> optionalBeitrag = beitragRepository.findById(beitragId);
        List<Archivbeitrag> archivbeitraege = new LinkedList<>();
        Beitrag beitrag = new Beitrag();
        if (optionalBeitrag.isPresent()) {
            beitrag = optionalBeitrag.get();
            archivbeitraege = archivbeitragRepository.findAllByBeitragOrderByErstellungsDatumDesc(beitrag);
        }
        model.addAttribute("beitrag", beitrag);
        model.addAttribute("archivbeitraege", archivbeitraege);
        model.addAttribute("beitragId", beitragId);
        model.addAttribute("dateFormatter",  DateTimeFormatter.ofLocalizedDateTime( FormatStyle.SHORT )
                .withLocale( Locale.getDefault() )
                .withZone( ZoneId.systemDefault() ));
        return "archiv";
    }
}
