package de.awacademy.webblogD2M.kommentar;

import de.awacademy.webblogD2M.beitrag.Beitrag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface KommentarRepository extends JpaRepository<Kommentar, Long> {
    List<Kommentar> findAllByOrderByErstellungsDatumAsc();
    List<Kommentar> findAllByBeitrag(Beitrag beitrag);
}
