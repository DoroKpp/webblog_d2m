package de.awacademy.webblogD2M.kommentar;

import de.awacademy.webblogD2M.beitrag.Beitrag;
import de.awacademy.webblogD2M.beitrag.BeitragRepository;
import de.awacademy.webblogD2M.user.User;
import de.awacademy.webblogD2M.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.lang.management.OperatingSystemMXBean;
import java.util.Optional;

@Controller
public class KommentarController {

    @Autowired
    KommentarRepository kommentarRepository;

    @Autowired
    BeitragRepository beitragRepository;

    @Autowired
    UserRepository userRepository;


    @GetMapping("/kommentar")
    public String create(Model model){
        model.addAttribute("kommentar", new KommentarDTO());
        return "redirect:/";
    }

    @PostMapping("/kommentar")
    public String create(@RequestParam("beitragId") long beitragId, @ModelAttribute("currentUser") User currentUser, @ModelAttribute("kommentar") KommentarDTO kommentarDTO){
        if(currentUser != null) {
            Kommentar kommentarEntity = new Kommentar(kommentarDTO.getText());
            kommentarEntity.setUser(currentUser);
            Optional<Beitrag> zugBeitrag = beitragRepository.findById(beitragId);
            if (zugBeitrag.isPresent()) {
                kommentarEntity.setBeitrag(zugBeitrag.get());
            }
            kommentarRepository.save(kommentarEntity);
        }
        return "redirect:/#beitrag-" + beitragId;
    }

    @PostMapping("/kommiLoeschen")
    public String kommiLoeschen(@RequestParam("kommentarId") long kommentarId, @ModelAttribute("currentUser") User currentUser){
        Optional<Kommentar> loeschKommi = kommentarRepository.findById(kommentarId);
        if(loeschKommi.isPresent() && (currentUser == loeschKommi.get().getUser() || currentUser.isAdmin())) {
            kommentarRepository.delete(loeschKommi.get());
        }
        return "redirect:/";
    }
}
