package de.awacademy.webblogD2M.beitrag;

import java.util.LinkedList;
import java.util.List;

public class BeitragDTO {

    private long id;
    private String text;
    private String titel;

    private List<Long> kategorieIdList = new LinkedList<>();

    public BeitragDTO(long id, String text, String titel) {
        this.id = id;
        this.text = text;
        this.titel = titel;
    }

    public BeitragDTO() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Long> getKategorieIdList() {
        return kategorieIdList;
    }

    public void setKategorieIdList(List<Long> kategorieIdList) {
        this.kategorieIdList= kategorieIdList;
    }
}
