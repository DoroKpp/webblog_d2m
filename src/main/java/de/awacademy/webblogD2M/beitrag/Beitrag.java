package de.awacademy.webblogD2M.beitrag;

import de.awacademy.webblogD2M.archivbeitrag.Archivbeitrag;
import de.awacademy.webblogD2M.kategorie.Kategorie;
import de.awacademy.webblogD2M.kommentar.Kommentar;
import de.awacademy.webblogD2M.user.User;

import javax.persistence.*;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Beitrag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Lob
    private String text;

    private String titel;
    private Instant erstellungsDatum;

    @OneToMany(mappedBy = "beitrag")
    private List<Kommentar> kommentare;

    @ManyToOne
    private User user;

    @ManyToMany
    private List<Kategorie> kategorien = new LinkedList<>();

    @OneToMany(mappedBy = "beitrag")
    private List<Archivbeitrag> archivbeitraege;

    public Beitrag() {
    }

    public Beitrag(String text, String titel) {
        this.text = text;
        this.titel = titel;
        this.erstellungsDatum = Instant.now();
    }
    /*GETTER*/

    public List<Kategorie> getKategorien() {
        return kategorien;
    }

    public User getUser() {
        return user;
    }

    public long getId() {
        return id;
    }

    public String getTitel() {
        return titel;
    }

    public String getText() {
        return text;
    }

    public Instant getErstellungsDatum() {
        return erstellungsDatum;
    }

    public List<Kommentar> getKommentare() {
        return kommentare;
    }

    public List<Archivbeitrag> getArchivbeitraege() {
        return archivbeitraege;
    }

    //SETTER
    public void setUser(User user) {
        this.user = user;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setErstellungsDatum(Instant erstellungsDatum) {
        this.erstellungsDatum = erstellungsDatum;
    }

}
