package de.awacademy.webblogD2M.beitrag;

import de.awacademy.webblogD2M.archivbeitrag.Archivbeitrag;
import de.awacademy.webblogD2M.archivbeitrag.ArchivbeitragRepository;
import de.awacademy.webblogD2M.kategorie.Kategorie;
import de.awacademy.webblogD2M.kategorie.KategorieRepository;
import de.awacademy.webblogD2M.kommentar.Kommentar;
import de.awacademy.webblogD2M.kommentar.KommentarRepository;
import de.awacademy.webblogD2M.user.User;
import de.awacademy.webblogD2M.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Controller
public class BeitragController {
    @Autowired
    BeitragRepository beitragRepository;

    @Autowired
    KategorieRepository kategorieRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    KommentarRepository kommentarRepository;

    @Autowired
    ArchivbeitragRepository archivbeitragRepository;

    @GetMapping("/beitrag")
    public String create(Model model){
        model.addAttribute("beitrag",new BeitragDTO());
        model.addAttribute("kategorien", kategorieRepository.findAll());
        return "/addBeitrag";
    }

    @PostMapping("/beitrag")
    public String create(@ModelAttribute("currentUser")User currentUser, @ModelAttribute("beitrag") BeitragDTO beitragDTO){
        if(currentUser == null){
            return "redirect:/login";
        }else if( !currentUser.isAdmin()){
            return "redirect:/login";
        }
        else{
            Beitrag beitragEntity = new Beitrag(beitragDTO.getText(),beitragDTO.getTitel());
            beitragEntity.setUser(currentUser);
            if (beitragDTO.getKategorieIdList().size() == 0) {
                beitragEntity.getKategorien().add(kategorieRepository.findById(1L).get());
            } else {
                for(long id : beitragDTO.getKategorieIdList()){
                    Optional<Kategorie> optionalKategorie = kategorieRepository.findById(id);
                    if (optionalKategorie.isPresent()) {
                        beitragEntity.getKategorien().add(optionalKategorie.get());
                    }
                }
            }
            beitragRepository.save(beitragEntity);
            return "redirect:/";
        }
    }

    @PostMapping("/beitragLoeschen")
    public String beitragLoeschen(@RequestParam("beitragId") long beitragId, @ModelAttribute("currentUser") User currentUser){
        Optional<Beitrag> loeschBeitrag = beitragRepository.findById(beitragId);
        if(loeschBeitrag.isPresent() && (currentUser == loeschBeitrag.get().getUser() || currentUser.isAdmin())) {
            List<Kommentar> zugKommentare = kommentarRepository.findAllByBeitrag(loeschBeitrag.get());
            for (Kommentar kommentar : zugKommentare) {
                kommentarRepository.delete(kommentar);
            }
            List<Archivbeitrag> zugArchivbeitraege = archivbeitragRepository.findAllByBeitrag(loeschBeitrag.get());
            for (Archivbeitrag archivbeitrag: zugArchivbeitraege) {
                archivbeitragRepository.delete(archivbeitrag);
            }
            beitragRepository.delete(loeschBeitrag.get());
        }
        return "redirect:/";
    }


    @GetMapping("/getEditBeitrag")
    public String edit(@RequestParam("beitragId") long beitragId, Model model) {
        Optional<Beitrag> zugBeitrag = beitragRepository.findById(beitragId);
        if (zugBeitrag.isPresent()) {
            BeitragDTO beitragDTO = new BeitragDTO(zugBeitrag.get().getId(), zugBeitrag.get().getText(), zugBeitrag.get().getTitel());
            for(Kategorie kategorie : zugBeitrag.get().getKategorien()) {
                beitragDTO.getKategorieIdList().add(kategorie.getId());
            }
            model.addAttribute("beitrag", beitragDTO);
            model.addAttribute("kategorien", kategorieRepository.findAll());
        }
        return "/editBeitrag";
    }



    @PostMapping("/saveBeitrag")
    public String beitragSpeichern(@RequestParam("beitragId") long beitragId, @ModelAttribute("beitrag") BeitragDTO beitragDTO, @ModelAttribute("currentUser") User currentUser) {
        Optional <Beitrag> optionalBeitrag = beitragRepository.findById(beitragId);
        if (optionalBeitrag.isPresent()) {
            Beitrag beitrag = optionalBeitrag.get();

            //Überprüfen, ob Text und Titel sich geändert haben
            if (!beitrag.getTitel().equals(beitragDTO.getTitel()) || !beitrag.getText().equals(beitragDTO.getText())) {
                //Eigenschaften an neuen "Archivbeitrag" übergeben
                Archivbeitrag archivbeitrag = new Archivbeitrag(beitrag.getText(), beitrag.getTitel(), beitrag.getErstellungsDatum(), beitrag.getUser(), beitrag);
                archivbeitragRepository.save(archivbeitrag);

                //Eigenschaften des Beitrags ändern
                beitrag.setText(beitragDTO.getText());
                beitrag.setTitel(beitragDTO.getTitel());
                beitrag.setUser(currentUser);
                beitrag.setErstellungsDatum(Instant.now());
            }
            //Änderung der Kategorien (wird nicht als "richtige" Änderung des Beitrags angesehen
            if (beitragDTO.getKategorieIdList().size() == 0) {
                beitrag.getKategorien().clear();
                Optional<Kategorie> optionalKategorie = kategorieRepository.findById(1L);
                if (optionalKategorie.isPresent()) {
                    beitrag.getKategorien().add(optionalKategorie.get());
                }
            } else {
                beitrag.getKategorien().clear();
                for (long id : beitragDTO.getKategorieIdList()) {
                    Optional<Kategorie> optionalKategorie = kategorieRepository.findById(id);
                    if (optionalKategorie.isPresent()) {
                        beitrag.getKategorien().add(optionalKategorie.get());
                    }
                }
            }
            beitragRepository.save(beitrag);
        }
        return "redirect:/";
    }
}

